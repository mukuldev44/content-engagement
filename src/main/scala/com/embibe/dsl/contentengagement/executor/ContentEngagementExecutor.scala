package com.embibe.dsl.contentengagement.executor

import org.apache.spark.sql.SparkSession
import com.embibe.dsl.contentengagement.questiondropoff.QuestionDropoff

/**
  * spark-submit --master yarn-client --driver-memory 2G --executor-memory 8G --num-executors 5 --executor-cores 5 --class com.embibe.dsl.contentengagement.executor.ContentEngagementExecutor /home/hadoop/mukul/content-engagement/target/scala-2.11/content-engagement-assembly-0.1.jar question-dropoff | tee logfile
  */
object ContentEngagementExecutor {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()

    val jobArg = args.headOption.getOrElse(throw new IllegalArgumentException("Please specify"))

    jobArg match {
      case x if x == "question-dropoff" => QuestionDropoff.execute
      case _ => throw new IllegalArgumentException(getClass.getSimpleName + " Following Job is not supported - " + jobArg)
    }
  }
}
