package com.embibe.dsl.contentengagement.questiondropoff

import com.typesafe.config.{ConfigFactory, Config}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.logging.log4j.LogManager

import com.embibe.dsl.contentengagement.questiondropoff.extractload.ExtractLoad.{getSqlDF, pushSqlDF, pushCSVDF}
import com.embibe.dsl.contentengagement.questiondropoff.transform.Transform._

/**
  * spark-shell --master yarn --driver-memory 2G --executor-memory 8G --num-executors 5 --executor-cores 5 --packages org.elasticsearch:elasticsearch-hadoop:6.3.1,com.typesafe:config:1.3.2,com.amazonaws:aws-java-sdk-s3:1.11.438,eu.bitwalker:UserAgentUtils:1.21,org.postgresql:postgresql:42.2.5,mysql:mysql-connector-java:8.0.13 --name dev_shell2
  */
object QuestionDropoff {
  def execute: Unit = {
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    val jobStartTime = spark.sparkContext.startTime
    LogManager.getRootLogger.info("Job started at - " + jobStartTime)
    // TODO: Add Job Start time to dataframe

    // Loading `questiondropoff` configuration file
    val conf = ConfigFactory.load("questiondropoff")
    LogManager.getRootLogger.info("loaded config -> " + conf.toString)

    // Get Input Config Details
    val inputConfig = conf.getConfig("input")
    val inputJdbcCred = inputConfig.getConfig("jdbc_cred")
    val inputExecConf = inputConfig.getConfig("execution")
    val inputExecType = inputExecConf.getString("type")
    val inputTableName = inputJdbcCred.getString("table_name")

    // Get Output Config Details
    val outputConfig = conf.getConfig("output")
    val outputJdbcCred = outputConfig.getConfig("jdbc_cred")
    val updateCsvOutputPath = (path: String) => if(path.endsWith("/")) path else path + "/"
    val outputCsvPath = outputConfig.getString("csv_output_path")
    val updatedOutputCsvPath = updateCsvOutputPath(outputCsvPath)

    // Getting Query Params
    val minusDaysInterval = inputExecType match {
      case x if x == "minus_days" => inputExecConf.getInt("interval")
      case _ => throw new IllegalArgumentException(getClass.getSimpleName + " - only input execution type - `minus_days` is supported")
    }
    // Creating Input SQL Query
    val sqlTableQuery = s"(select * from $inputTableName where created_at >= NOW() - interval '$minusDaysInterval day') AS $inputTableName"
    // Extracting SQL DataFrame and Persisting it In-Memory
    val attemptsMonthDF = getSqlDF(spark, inputJdbcCred, sqlTableQuery).repartition(100)
    attemptsMonthDF.cache()
    // Filter Valid Attempts
    val badgeNormalizedPracticeAttempts = filterValidAttemptsDF(attemptsMonthDF)
    // Implement Question Dropoff Logic for Practice Attempts
    val monthSeqDF = practiceSequenceQuestionDropoff(badgeNormalizedPracticeAttempts).filter(col("sequence") <= 50)
    // Get Aggregated Question Dropoff at Sequence Levels and broadcasting it
    val seqAnalysis = getSequenceAnalysis(monthSeqDF)
    seqAnalysis.cache()
    val sequenceBroadcastMap = getSeqAnalysisBroadcastVariable(seqAnalysis)
    // Adding High Dropoff Flag
    val flagQuestionSeqDropoffDF = getQuestionSequenceFlagDF(monthSeqDF, sequenceBroadcastMap)
    // Getting Flag on Question Level
    val flagQuesDropoffDF = getQuestionDropoffFlagDF(flagQuestionSeqDropoffDF, jobStartTime)

    // Pushing Data to Sql
    pushSqlDF(flagQuesDropoffDF, outputJdbcCred)
    // Pushing Backup to S3
    pushCSVDF(seqAnalysis, updatedOutputCsvPath + "sequence-analysis/" + jobStartTime)
    pushCSVDF(flagQuestionSeqDropoffDF, updatedOutputCsvPath + "question-sequence-dropoff/" + jobStartTime)
  }
}
