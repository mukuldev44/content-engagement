package com.embibe.dsl.contentengagement.questiondropoff.extractload

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import com.typesafe.config.{ConfigFactory, Config}

/** Question Dropoff Extract Load Object
  *
  * https://medium.com/@radek.strnad/tips-for-using-jdbc-in-apache-spark-sql-396ea7b2e3d3
  *
  * Created By Mukul Dev
  */
object ExtractLoad {
  /**
    *
    */
  private val jdbcCredParser = (jdbcCred: Config) => {
    JdbcCred(
      driver = jdbcCred.getString("driver"),
      url = jdbcCred.getString("url"),
      user = jdbcCred.getString("user"),
      password = jdbcCred.getString("password"),
      table_name = jdbcCred.getString("table_name")
    )
  }

  /**
    *
    * @param spark
    * @param jdbcCred
    * @param table_query
    * @return
    */
  def getSqlDF(spark: SparkSession, jdbcCred: Config, table_query: String): DataFrame = {
    val inputJdbcCred = jdbcCredParser(jdbcCred)
    spark.read
      .format("jdbc")
      .option("url", inputJdbcCred.url)
      .option("dbtable", table_query)
      .option("user", inputJdbcCred.user)
      .option("driver", inputJdbcCred.driver)
      .option("password", inputJdbcCred.password)
      .load()
  }

  /**
    *
    * @param df
    * @param jdbcCred
    */
  def pushSqlDF(df: DataFrame, jdbcCred: Config): Unit = {
    val outputJdbcCred = jdbcCredParser(jdbcCred)

    val jdbc_props = new java.util.Properties
    jdbc_props.setProperty("user", outputJdbcCred.user)
    jdbc_props.setProperty("password", outputJdbcCred.password)
    jdbc_props.setProperty("driver", outputJdbcCred.driver)

    df
      .write
      .mode(SaveMode.Append)
      .jdbc(outputJdbcCred.url, outputJdbcCred.table_name, jdbc_props)
  }

  /**
    *
    * @param df
    * @param path
    */
  def pushCSVDF(df: DataFrame, path: String): Unit = {
    df.coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(path)
  }
}

case class JdbcCred(
                     driver: String,
                     url: String,
                     user: String,
                     password: String,
                     table_name: String
                   )
