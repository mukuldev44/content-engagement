package com.embibe.dsl.contentengagement.questiondropoff.transform

import org.apache.logging.log4j.LogManager
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

/** Question Dropoff Transformation Methods
  *
  * Created By Mukul Dev
  */
object Transform {
  /** getSequenceAnalysis
    *
    * @param df
    * @return
    */
  def getSequenceAnalysis(df: DataFrame): DataFrame = {
    val seqAnalysis = df
      .groupBy("sequence")
      .agg(
        sum("dropped_sessions").as("dropped_sessions"),
        sum("total_sessions").as("total_sessions"),
        sum("dropped_sessions_non_attempt").as("dropped_sessions_non_attempt"),
        count(col("question_code")).as("distinct_questions")
      )
      .withColumn("avg_dropoff_rate", round((col("dropped_sessions")/col("total_sessions")) * 100, 2))
      .withColumn("avg_dropoff_rate_non_attempt", round((col("dropped_sessions_non_attempt")/col("total_sessions")) * 100, 2))

    seqAnalysis
  }

  /** getSeqAnalysisBroadcastVariable
    *
    * @param df
    * @return
    */
  def getSeqAnalysisBroadcastVariable(df: DataFrame): Broadcast[Map[String, Map[String, String]]] = {
    val zippedArrayMap = df
      .collect()
      .map(r => df.columns.zip(r.toSeq.map(_.toString)).toMap)

    val sequenceMap = zippedArrayMap.map(e => e("sequence") -> e).toMap
    df.sparkSession.sparkContext.broadcast(sequenceMap)
  }

  /** practiceSequenceQuestionDropoff
    *
    * @param df
    * @return
    */
  def practiceSequenceQuestionDropoff(df: DataFrame): DataFrame = {
    val recordsCount = df.count()
    LogManager.getRootLogger.info("Fetched Records - " + recordsCount)

    val totalSessionsDF = df
      .groupBy("question_code", "sequence").agg(countDistinct("session_id").as("total_sessions"))
      .alias("totalSessionsDF")

    val w1 = Window.partitionBy("session_id").orderBy(desc("sequence"))

    val lastQuestionDF = df
      .withColumn("rn", row_number().over(w1))
      .filter(col("rn") === 1)
      .drop("rn")

    val droppedSessionsDF = lastQuestionDF
      .groupBy("question_code", "sequence")
      .agg(
        count("session_id").as("dropped_sessions"),
        sum(when(col("correctness").isNull, 1).otherwise(0)).as("dropped_sessions_non_attempt")
      )
      .alias("droppedSessionsDF")

    val outDF = totalSessionsDF.join(droppedSessionsDF,
      trim(col("totalSessionsDF.question_code")) === trim(col("droppedSessionsDF.question_code")) &&
        trim(col("totalSessionsDF.sequence")) === trim(col("droppedSessionsDF.sequence")), "outer")
      .drop(col("droppedSessionsDF.question_code"))
      .drop(col("droppedSessionsDF.sequence"))
      .na.fill(0)

    outDF
      .withColumn("dropoff_rate", round( (col("dropped_sessions")/col("total_sessions")) * 100, 2))
      .withColumn("dropoff_rate_non_attempt", round( (col("dropped_sessions_non_attempt")/col("total_sessions")) * 100, 2))
      .orderBy(desc("dropoff_rate"), desc("total_sessions"), desc("dropped_sessions"))
  }

  /** getSeqWtAvgUDF
    *
    * @param sequenceBroadcastMap
    * @return
    */
  def getSeqWtAvgUDF(sequenceBroadcastMap: Broadcast[Map[String, Map[String, String]]]) =
    udf({ sequenceSet: Seq[Int] =>
      val sequenceValueMap = sequenceBroadcastMap.value
      val filterSequenceValues = sequenceSet.map { s =>
        val sequenceValue = sequenceValueMap(s.toString)
        val droppedSession = sequenceValue("dropped_sessions").toLong
        val totalSession = sequenceValue("total_sessions").toLong

        (droppedSession, totalSession)
      }.toList
      val droppedTotalTuple = filterSequenceValues.reduce((a, b) => (a._1 + b._1, a._2 + b._2))
      (droppedTotalTuple._1.toDouble / droppedTotalTuple._2) * 100
    })

  /** getQuestionSequenceFlagDF
    *
    * @param monthSeqDF
    * @param sequenceBroadcastMap
    * @return
    */
  def getQuestionSequenceFlagDF(monthSeqDF: DataFrame, sequenceBroadcastMap:  Broadcast[Map[String, Map[String, String]]]): DataFrame = {

    val questionWindow = Window.partitionBy("question_code")

    val questionSeqAggDropoffDF = monthSeqDF
      .withColumn("sequence_set", collect_set("sequence").over(questionWindow))
      .withColumn("wt_avg_dropoff_rate", (col("total_sessions") / sum("total_sessions").over(questionWindow)) * col("dropoff_rate"))
      .withColumn("question_wt_avg_dropoff_rate", sum("wt_avg_dropoff_rate").over(questionWindow))
      .withColumn("seq_wt_avg", getSeqWtAvgUDF(sequenceBroadcastMap)(col("sequence_set")))

    val flagQuestionSeqDropoffDF = questionSeqAggDropoffDF
      .withColumn("questionDropoffFlag", when(col("question_wt_avg_dropoff_rate") > col("seq_wt_avg") * 2, "fail").otherwise("pass"))

    flagQuestionSeqDropoffDF
  }

  /** filterValidAttemptsDF
    *
    * @param df
    * @return
    */
  def filterValidAttemptsDF(df: DataFrame): DataFrame = {
    val sessionWindow = Window.partitionBy("session_id")

    df.withColumn("wrong_attempt_count", sum(when(col("badge") === "wasted-attempt" || col("badge") === "non-attempt", 1).otherwise(0)).over(sessionWindow))
      .withColumn("session_count", count("*").over(sessionWindow))
      .filter((col("session_count") / 2) >= col("wrong_attempt_count"))
      .filter(col("sequence") <= 51)
  }

  /** getQuestionDropoffFlagDF
    *
    * @param df
    * @param jobStartTime
    * @return
    */
  def getQuestionDropoffFlagDF(df: DataFrame, jobStartTime: Long): DataFrame = {
    df
      .withColumn("execution_time", lit(jobStartTime))
      .dropDuplicates("question_code")
      .drop("sequence")
      .drop("total_sessions")
      .drop("dropped_sessions")
      .drop("dropped_sessions_non_attempt")
      .drop("dropoff_rate")
      .drop("dropoff_rate_non_attempt")
      .drop("sequence_set")
      .drop("wt_avg_dropoff_rate")
  }
}
