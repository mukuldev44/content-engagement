package com.embibe.dsl.contentengagement.conceptengagement

import java.sql.Date

import com.embibe.dsl.core.aws.s3.ObjectLister.getS3Objects
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.collection.JavaConverters._

/**
  * Users
  * Time-spent
  * Average Time-spent
  * Pageviews
  * Retention on Embibe after visiting this concept
  * Video clicked
  * Exit page
  */
object ContentEngagementExecutor {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    // ---- Extract ----
    // COV
    val s3BucketName = "dsl-datastore"
    val covDirPrefix = "data/content/cov/prod/exam_wise_cov_data/20190101/"
    val covPathList = getS3Objects(s3BucketName, covDirPrefix).filter(_.contains(".json")).map(s"s3a://$s3BucketName/" + _)
    val covDF = spark.read.schema(covSchema).json(covPathList: _*)
    covDF.persist(StorageLevel.MEMORY_ONLY_SER)
    // Broadcast Variables
    val goalCOVDF = getUnitIdCodeMap(covDF, "goal_id", "goal_code", "goal_name")
    val examCOVDF = getUnitIdCodeMap(covDF, "exam_id", "exam_code", "exam_name")
    val subjectCOVDF = getUnitIdCodeMap(covDF, "subject_id", "subject_code", "subject_name")
    val unitCOVDF = getUnitIdCodeMap(covDF, "unit_id", "unit_code", "unit_name")
    val chapterCOVDF = getUnitIdCodeMap(covDF, "chapter_id", "chapter_code", "chapter_name")
    val conceptCOVDF = getUnitIdCodeMap(covDF, "concept_id", "concept_code", "concept_name")
    val covDS = covDF
      .select("content_code", "goal_code", "goal_name", "exam_code", "exam_name", "subject_code", "subject_name", "unit_code", "unit_name", "chapter_code", "chapter_name", "concept_code", "concept_name")
      .as[CovInputSchema]
    val contentCovList = getContentCovList(covDS)
    covDF.unpersist()

    // Config
    val config = getConfig
    val analysisConfig = config.getConfig("analysis")
    val analysisRangeConfig = analysisConfig.getConfig("range")
    val startDate = new DateTime(Date.valueOf(analysisRangeConfig.getString("start")).getTime)
    val endDate = new DateTime(Date.valueOf(analysisRangeConfig.getString("end")).getTime)

    // Input Clickstream Data
    val rearchDirPrefix = "data/ingested/clickstream/raw/rearch/"
    val nonRearchDirPrefix = "data/ingested/clickstream/raw/non-rearch/"
    val rearchInputPathList = getS3InputPathList(s3BucketName, rearchDirPrefix, startDate, endDate)
    val nonRearchInputPathList = getS3InputPathList(s3BucketName, nonRearchDirPrefix, startDate, endDate)
    val inputPathList = (rearchInputPathList ++ nonRearchInputPathList).map(s"s3a://$s3BucketName/" + _)

    // Reading Input files using spark
    val inputDF = spark.read.schema(inputSchema).json(inputPathList: _*)
    val inputDS = getInputDS(inputDF)

    inputDS.filter(col("url").contains("/study/")).select("referrer").show(100, false)


    inputDS.filter(col("url").contains("/study/") && col("referrer").contains("www.google."))

    inputDS.filter(col("url").contains("/study/") && col("url").contains("entity_code=KTII18"))

    inputDS.filter(col("url").contains("/study/") && col("referrer").contains("embibe.com/search"))

    inputDS.filter(col("url").contains("/study/") && col("url").contains("entity_code=KTII18") && col("referrer").contains("embibe.com/search"))

    // ---- Transform ----
    // mapping url with learning map
    val clickStreamLearningMapDF = getUrlMappedDS(inputDS, "url", contentCovList, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF, conceptCOVDF)

    val timestampDiffDF = getTimestampDiffDF(clickStreamLearningMapDF)
    val exitEventDF = getLastEventOfUser(timestampDiffDF)
    val outDF = calculateConceptTimespent(exitEventDF)

    outDF
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv("s3a://dsl-datastore/data/content-engagement/output/concept-engagement/2019-01-01--2019-01-07_1")


    // Joining Month and Week DataFrame
    val monthDF = spark.read.option("header", "true").csv("s3a://dsl-datastore/data/content-engagement/output/concept-engagement/2018-12-26/30days")
    val weekDF = spark.read.option("header", "true").csv("s3a://dsl-datastore/data/content-engagement/output/concept-engagement/2018-12-26/7days")

    val joinDF = monthDF.join(weekDF, monthDF("concept_code") === weekDF("concept_code"), "outer")
      .withColumn("total_timespent_month", monthDF("total_timespent"))
      .withColumn("distinct_users_count_month", monthDF("distinct_users_count"))
      .withColumn("toal_page_views_month", monthDF("toal_page_views"))
      .withColumn("total_video_clicked_month", monthDF("total_video_clicked"))
      .withColumn("total_exit_events_month", monthDF("total_exit_events"))
      .withColumn("total_timespent_week", weekDF("total_timespent"))
      .withColumn("distinct_users_count_week", weekDF("distinct_users_count"))
      .withColumn("toal_page_views_week", weekDF("toal_page_views"))
      .withColumn("total_video_clicked_week", weekDF("total_video_clicked"))
      .withColumn("total_exit_events_week", weekDF("total_exit_events"))
      .drop(weekDF("concept_code"))
      .drop(weekDF("concept_name"))
      .drop(weekDF("total_timespent"))
      .drop(weekDF("distinct_users_count"))
      .drop(weekDF("toal_page_views"))
      .drop(weekDF("total_video_clicked"))
      .drop(weekDF("total_exit_events"))
      .drop(monthDF("total_timespent"))
      .drop(monthDF("distinct_users_count"))
      .drop(monthDF("toal_page_views"))
      .drop(monthDF("total_video_clicked"))
      .drop(monthDF("total_exit_events"))

    joinDF
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv("s3a://dsl-datastore/data/content-engagement/output/concept-engagement/2018-12-26/join_v1")
  }

  def calculateConceptTimespent(df: DataFrame) = {
    df
      .filter(""" concept_code is not null and concept_code != "" """)
      .groupBy("concept_code", "concept_name")
      .agg(
        sum("timestamp_diff").as("total_timespent"),
        countDistinct(col("user_id")).as("distinct_users_count"),
        countDistinct(col("user_id"), col("custom_session_id")).as("distinct_sessions_count"),
        sum(when(col("event_name") === "page load", 1).otherwise(0)).as("toal_page_views"),
        sum(when(col("event_name") === "play/pause video" && col("video_state") === "play" && col("video_time_position") < lit(0.1), 1).otherwise(0)).as("total_video_clicked"),
        sum("is_last_event").as("total_exit_events")
      )
  }

  def getLastEventOfUser(df: DataFrame) = {
    val w = Window.partitionBy("user_id", "custom_session_id").orderBy(desc("timestamp"))

    df
      .withColumn("rn", row_number().over(w))
      .withColumn("is_last_event", when(col("rn") === 1, 1).otherwise(0))
  }

  def getTimestampDiffDF(pageLoadClickstreamDS: Dataset[UrlMappedSchema]) = {
    val timestampColumn = to_timestamp(col("timestamp"))
    val timestampEpoch = timestampColumn.cast(LongType)

    val w1 = Window.partitionBy(col("user_id")).orderBy(timestampColumn)

    val newSession = (coalesce(timestampEpoch - lag(timestampEpoch, 1).over(w1), lit(0)) >= 30 * 60).cast("bigint")
    val sessionizedDF = pageLoadClickstreamDS.withColumn("custom_session_id", sum(newSession).over(w1))

    val w2 = Window.partitionBy(col("user_id"), col("url"), col("custom_session_id")).orderBy(timestampColumn)

    val diffDF = sessionizedDF.withColumn("timestamp_diff", timestampEpoch - lag(timestampEpoch, 1).over(w2))
    diffDF
  }

  def getUrlMappedDS(
                      ds: Dataset[InputSchema],
                      analyzeField: String,
                      contentCovList: Broadcast[scala.List[CovInputSchema]],
                      goalCOVDF: Broadcast[Map[String, Map[String, String]]],
                      examCOVDF: Broadcast[Map[String, Map[String, String]]],
                      subjectCOVDF: Broadcast[Map[String, Map[String, String]]],
                      unitCOVDF: Broadcast[Map[String, Map[String, String]]],
                      chapterCOVDF: Broadcast[Map[String, Map[String, String]]],
                      conceptCOVDF: Broadcast[Map[String, Map[String, String]]]
                    ) = {
    import ds.sparkSession.implicits._

    ds.flatMap{ is =>

      val analyzeString = analyzeField match {
        case x if x.toLowerCase().trim == "url" => is.url
        case x if x.toLowerCase().trim == "referrer" => is.referrer
      }

      val (urlType, requiredTokenMap) = urlParser(analyzeString, getConfig)

      val urlMappedInputRowList = urlType match {
        case x if urlType.contains("practice") => mapDistinctAttributes(is, requiredTokenMap, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF)
        case x if urlType.contains("test") => mapDistinctAttributes(is, requiredTokenMap, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF)
        case x if urlType.contains("study") => mapAllContentAttributes(is, requiredTokenMap, contentCovList)
        case _ => List(UrlMappedSchema(is.user_id, is.url, is.referrer, is.user_agent, is.session_id, is.event_code, is.event_name, is.event_type, is.video_state, is.video_url, is.video_time_position, is.content_id, is.content_type, is.timestamp, is.date, is.year, is.month, is.week, is.month_day, is.hour, "", "", "", "", "", "", "", "", "", "", "", ""))
      }
      urlMappedInputRowList
    }
  }

  def urlParser(url: String, config: Config) = {
    val urlParserConfig = config.getConfig("url_parser")
    val invalidUrlList = urlParserConfig.getStringList("invalid_url_list").asScala.toList

    if(url != null && url.nonEmpty && ! invalidUrlList.contains(url)) {
      val prefixStripUrl = url.trim.toLowerCase.stripPrefix("https://")
      val urlParserRuleList = urlParserConfig.getConfigList("rules").asScala.toList

      val urlParserRuleConfig = urlParserRuleList.find { parserConfig =>
        val separatorList = parserConfig.getStringList("token_separator_list").asScala.map(_.charAt(0)).toArray
        val tokenizedUrl = prefixStripUrl.split(separatorList).toList.tail
        val identifierConfigList = parserConfig.getConfigList("identifier").asScala.toList
        val bool = identifierConfigList.map { identifierConfig =>
          // removing `<?>.embibe.com`
          val identifierTokenPos = identifierConfig.getInt("token_id") - 1
          val identifierTokenVal = identifierConfig.getString("value")
          if (tokenizedUrl.nonEmpty && tokenizedUrl.length > identifierTokenPos && tokenizedUrl(identifierTokenPos) == identifierTokenVal) true else false
        }.reduce(_ & _)
        bool
      }

      val formatToken = (token: String) => token.trim.toLowerCase.replace("-", " ")

      if (urlParserRuleConfig.isDefined) {
        val parserConfig = urlParserRuleConfig.get
        val separatorList = parserConfig.getStringList("token_separator_list").asScala.map(_.charAt(0)).toArray
        val tokenSequence = parserConfig.getStringList("token_sequence").asScala.toList
        val requiredTokens = parserConfig.getStringList("tokens_required").asScala.toList
        val parserName = parserConfig.getString("parser_name")

        val tokenizedUrl = prefixStripUrl.split(separatorList).toList.tail
        val zippedTokenMap = tokenSequence.zip(tokenizedUrl).toMap
        val tokenMap = requiredTokens.map(token => (token, formatToken(zippedTokenMap.getOrElse(token, "")))).toMap
        (parserName, tokenMap)
      } else {
        ("_dummy", Map.empty[String, String])
      }
    } else {
      ("_dummy", Map.empty[String, String])
    }
  }

  def mapDistinctAttributes(
                             row: InputSchema,
                             tokenMap: Map[String, String],
                             goalCOVDF: Broadcast[Map[String, Map[String, String]]],
                             examCOVDF: Broadcast[Map[String, Map[String, String]]],
                             subjectCOVDF: Broadcast[Map[String, Map[String, String]]],
                             unitCOVDF: Broadcast[Map[String, Map[String, String]]],
                             chapterCOVDF: Broadcast[Map[String, Map[String, String]]]
                           ): List[UrlMappedSchema] = {
    // Goal
    val goalTokenNameOption = tokenMap.get("goal")
    val (goalName, goalCode) = getDistinctTokenCode(goalTokenNameOption, goalCOVDF)
    // Exam
    val examTokenNameOption = tokenMap.get("exam")
    val (examName, examCode) = getDistinctTokenCode(examTokenNameOption, examCOVDF)
    // Subject
    val subjectTokenNameOption = tokenMap.get("subject")
    val (subjectName, subjectCode) = getDistinctTokenCode(subjectTokenNameOption, subjectCOVDF)
    // Unit
    val unitTokenNameOption = tokenMap.get("unit")
    val (unitName, unitCode) = getDistinctTokenCode(unitTokenNameOption, unitCOVDF)
    // Chapter
    val chapterTokenNameOption = tokenMap.get("chapter")
    val (chapterName, chapterCode) = getDistinctTokenCode(chapterTokenNameOption, chapterCOVDF)
    // Concept
    val (conceptName, conceptCode) = ("", "")

    List(
      UrlMappedSchema(
        user_id = row.user_id,
        url = row.url,
        referrer = row.referrer,
        user_agent = row.user_agent,
        session_id = row.session_id,
        event_code = row.event_code,
        event_name = row.event_name,
        event_type = row.event_type,
        video_state = row.video_state,
        video_url = row.video_url,
        video_time_position = row.video_time_position,
        content_id = row.content_id,
        content_type = row.content_type,
        timestamp = row.timestamp,
        date = row.date,
        year = row.year,
        month = row.month,
        week = row.week,
        month_day = row.month_day,
        hour = row.hour,
        goal_code =  goalCode,
        goal_name = goalName,
        exam_code = examCode,
        exam_name = examName,
        subject_code = subjectCode,
        subject_name = subjectName,
        unit_code = unitCode,
        unit_name = unitName,
        chapter_code = chapterCode,
        chapter_name = chapterName,
        concept_code = conceptCode,
        concept_name = conceptName
      )
    )
  }

  def getDistinctTokenCode(tokenNameOption: Option[String], tokenNameCodeMapBroadcast: Broadcast[Map[String, Map[String, String]]]) = {

    val (tokenName, tokenCode) = if(tokenNameOption.isDefined && tokenNameOption.get != null && tokenNameOption.nonEmpty) {
      val tokenNameValue = tokenNameOption.get
      val tokenNameCodeMapValue = tokenNameCodeMapBroadcast.value
      val tokenMapOption = tokenNameCodeMapValue.get(tokenNameValue)
      if(tokenMapOption.isDefined) {
        (tokenNameValue, tokenMapOption.get("codeColumn"))
      } else {
        (tokenNameValue, "")
      }
    } else {
      ("", "")
    }

    (tokenName, tokenCode)
  }

  def mapAllContentAttributes(
                               row: InputSchema,
                               tokenMap: Map[String, String],
                               contentCovList: Broadcast[scala.List[CovInputSchema]]
                             ): List[UrlMappedSchema] = {
    val contentCovListValue = contentCovList.value
    val entityCodeOption = tokenMap.get("entity-code")

    val filterCovData = if(entityCodeOption.isDefined) {
      val entityCode = entityCodeOption.get.trim.toLowerCase()
      contentCovListValue.filter { cov =>
        if(cov.content_code != null && cov.content_code.nonEmpty) {
          cov.content_code.trim.toLowerCase == entityCode
        } else {
          false
        }
      }
    } else {
      List.empty[CovInputSchema]
    }
    if(filterCovData.isEmpty){
      List(
        UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          referrer = row.referrer,
          user_agent = row.user_agent,
          session_id = row.session_id,
          event_code = row.event_code,
          event_name = row.event_name,
          event_type = row.event_type,
          video_state = row.video_state,
          video_url = row.video_url,
          video_time_position = row.video_time_position,
          content_id = row.content_id,
          content_type = row.content_type,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          month_day = row.month_day,
          hour = row.hour,
          goal_code = "",
          goal_name = "",
          exam_code = "",
          exam_name = "",
          subject_code = "",
          subject_name = "",
          unit_code = "",
          unit_name = "",
          chapter_code = "",
          chapter_name = "",
          concept_code = "",
          concept_name = ""
        )
      )
    }
    else {
      filterCovData.map { cov =>
        UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          referrer = row.referrer,
          user_agent = row.user_agent,
          session_id = row.session_id,
          event_code = row.event_code,
          event_name = row.event_name,
          event_type = row.event_type,
          video_state = row.video_state,
          video_url = row.video_url,
          video_time_position = row.video_time_position,
          content_id = row.content_id,
          content_type = row.content_type,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          month_day = row.month_day,
          hour = row.hour,
          goal_code = cov.goal_code,
          goal_name = cov.goal_name,
          exam_code = cov.exam_code,
          exam_name = cov.exam_name,
          subject_code = cov.subject_code,
          subject_name = cov.subject_name,
          unit_code = cov.unit_code,
          unit_name = cov.unit_name,
          chapter_code = cov.chapter_code,
          chapter_name = cov.chapter_name,
          concept_code = cov.concept_code,
          concept_name = cov.concept_name
        )
      }
    }
  }

  def getUnitIdCodeMap(df: DataFrame, idColumn: String, codeColumn: String, nameColumn: String) = {
    val distinctDF = df.select(idColumn, codeColumn, nameColumn).distinct()
      .filter(col(codeColumn).isNotNull && col(codeColumn) =!= "")
      .withColumnRenamed(codeColumn, "codeColumn")
      .withColumnRenamed(idColumn, "idColumn")
      .withColumnRenamed(nameColumn, "nameColumn")

    val distinctMapArray = distinctDF
      .collect()
      .map(r => Map(distinctDF.columns.zip(r.toSeq.map(_.toString)): _*))
    val distinctMap = Map(distinctMapArray.map(p => (p("idColumn").trim.toLowerCase, p)):_*)

    df.sparkSession.sparkContext.broadcast(distinctMap)
  }

  def getContentCovList(df: Dataset[CovInputSchema]): Broadcast[scala.List[CovInputSchema]] = {
    val distinctMapList = df
      .collect()
      .toList

    df.sparkSession.sparkContext.broadcast(distinctMapList)
  }

  def getInputDS(df: DataFrame): Dataset[InputSchema] = {
    import df.sparkSession.implicits._

    val inputFlatDF = df
      .withColumn("user_id", trim(coalesce(col("userId"), col("properties.user_id"))))
      .withColumn("url", col("context.page.url"))
      .withColumn("referrer", col("context.page.referrer"))
      .withColumn("user_agent", col("context.userAgent"))
      .withColumn("session_id", col("properties.session_id"))
      .withColumn("event_code", col("properties.event_code"))
      .withColumn("event_name", col("properties.event_name"))
      .withColumn("event_type", col("properties.event_type"))
      .withColumn("video_state", col("properties.extra_params.state"))
      .withColumn("video_url", col("properties.extra_params.url"))
      .withColumn("video_time_position", col("properties.extra_params.time_position"))
      .withColumn("content_id", col("properties.extra_params.content_id"))
      .withColumn("content_type", col("properties.extra_params.content_type"))
      .drop("context")
      .drop("properties")
      .drop("userId")

    val dateDF = inputFlatDF
      .withColumn("video_time_position", when(col("video_time_position").isNull, 0).otherwise(col("video_time_position")))
      .withColumn("date", to_date(col("timestamp")))
      .withColumn("year", year(col("date")))
      .withColumn("month", month(col("date")))
      .withColumn("week", weekofyear(col("date")))
      .withColumn("month_day", dayofmonth(col("date")))
      .withColumn("hour", hour(to_timestamp(col("timestamp"))))
      .na.fill(0)

    val inputDS = dateDF.as[InputSchema]

    inputDS
  }

  def getS3InputPathList(bucketName: String, dirPrefix: String, startDate: DateTime, endDate: DateTime): List[String] = {
    assert(startDate.compareTo(endDate) == -1 || startDate.compareTo(endDate) == 0,
      getClass.getSimpleName + " - Error: Input Start Date should be less than or equal to End Date")

    val noOfDays = org.joda.time.Days.daysBetween(startDate, endDate).getDays
    val s3ObjectsListList = (0 to noOfDays).map{ dayNo =>
      val formatInt = (x: Int) => if (x < 10) "0" + x else  x.toString
      val indexDate = startDate.plusDays(dayNo)
      val year = indexDate.getYear
      val month = formatInt(indexDate.getMonthOfYear)
      val day = formatInt(indexDate.dayOfMonth.get())

      val updatedDirPrefix = if(dirPrefix.endsWith("/")){
        dirPrefix + year + "/" + month + "/" + day
      } else {
        dirPrefix + "/" + year + "/" + month + "/" + day
      }

      getS3Objects(bucketName, updatedDirPrefix)
    }

    s3ObjectsListList.reduceOption(_ ++ _).getOrElse(List.empty[String])
  }

  val covSchema: StructType = {
    val rootSchema = StructType(
      Seq(
        StructField("content_code", StringType),
        StructField("goal_code", StringType),
        StructField("goal_name", StringType),
        StructField("goal_id", StringType),
        StructField("exam_code", StringType),
        StructField("exam_name", StringType),
        StructField("exam_id", StringType),
        StructField("subject_code", StringType),
        StructField("subject_name", StringType),
        StructField("subject_id", StringType),
        StructField("unit_code", StringType),
        StructField("unit_name", StringType),
        StructField("unit_id", StringType),
        StructField("chapter_code", StringType),
        StructField("chapter_name", StringType),
        StructField("chapter_id", StringType),
        StructField("concept_code", StringType),
        StructField("concept_name", StringType),
        StructField("concept_id", StringType)
      )
    )
    rootSchema
  }

  val inputSchema: StructType = {
    val propertiesExtraParamsSchema = StructType(
      Seq(
        StructField("state", StringType),
        StructField("url", StringType),
        StructField("time_position", DoubleType),
        StructField("content_id", StringType),
        StructField("content_type", StringType)
      )
    )
    val propertiesSchema = StructType(
      Seq(
        StructField("user_id", StringType),
        StructField("session_id", StringType),
        StructField("event_code", StringType),
        StructField("event_name", StringType),
        StructField("event_type", StringType),
        StructField("extra_params", propertiesExtraParamsSchema)
      )
    )
    val pageSchema = StructType(
      Seq(
        StructField("url", StringType),
        StructField("referrer", StringType)
      )
    )
    val contextSchema = StructType(
      Seq(
        StructField("userAgent", StringType),
        StructField("page", pageSchema)
      )
    )
    val rootSchema = StructType(
      Seq(
        StructField("userId", StringType),
        StructField("timestamp", StringType),
        StructField("properties", propertiesSchema),
        StructField("context", contextSchema)
      )
    )
    rootSchema
  }

  def getConfig: Config = {
    val urlParserString =
      """
        |{
        |	"analysis": {
        |		"range": {
        |			"start": "2019-01-07",
        |			"end": "2019-01-07"
        |		}
        |	},
        |	"url_parser": {
        |		"invalid_url_list": [
        |			"https://www.embibe.com/",
        |			"https://www.embibe.com/404",
        |			"https://www.embibe.com/ai",
        |			"https://www.embibe.com/landing",
        |			"https://www.embibe.com/rankup",
        |			"https://www.embibe.com/search"
        |		],
        |		"rules": [{
        |				"parser_name": "practice",
        |				"identifier": [{
        |					"token_id": 2,
        |					"value": "practice"
        |				}],
        |				"token_separator_list": ["/"],
        |				"token_sequence": ["goal", "practice", "solve", "exam", "subject", "unit", "chapter"],
        |				"tokens_required": ["goal", "exam", "subject", "unit", "chapter"]
        |			},
        |			{
        |				"parser_name": "chapterwise-test",
        |				"identifier": [{
        |						"token_id": 2,
        |						"value": "test"
        |					},
        |					{
        |						"token_id": 4,
        |						"value": "chapterwise-test"
        |					}
        |				],
        |				"token_separator_list": ["/"],
        |				"token_sequence": ["goal", "test", "exam", "chapterwise-test", "subject", "chapter"],
        |				"tokens_required": ["goal", "exam", "subject", "chapter"]
        |			},
        |			{
        |				"parser_name": "part-test",
        |				"identifier": [{
        |						"token_id": 2,
        |						"value": "test"
        |					},
        |					{
        |						"token_id": 4,
        |						"value": "part-test"
        |					}
        |				],
        |				"token_separator_list": ["/"],
        |				"token_sequence": ["goal", "test", "exam", "chapterwise-test", "subject", "unit"],
        |				"tokens_required": ["goal", "exam", "subject", "unit"]
        |			},
        |			{
        |				"parser_name": "other-test",
        |				"identifier": [{
        |					"token_id": 2,
        |					"value": "test"
        |				}],
        |				"token_separator_list": ["/"],
        |				"token_sequence": ["goal", "test", "exam", "test-type", "test-type"],
        |				"tokens_required": ["goal", "exam"]
        |			},
        |			{
        |				"parser_name": "study",
        |				"identifier": [{
        |					"token_id": 1,
        |					"value": "study"
        |				}],
        |				"token_separator_list": ["/", "?", "="],
        |				"token_sequence": ["study", "entity-name-type", "entity_code", "entity-code"],
        |				"tokens_required": ["entity-code"]
        |			}
        |		]
        |	}
        |}
      """.stripMargin

    ConfigFactory.parseString(urlParserString)
  }
}


case class InputSchema(
                        user_id: String,
                        url: String,
                        referrer: String,
                        user_agent: String,
                        session_id: String,
                        event_code: String,
                        event_name: String,
                        event_type: String,
                        video_state: String,
                        video_url: String,
                        video_time_position: Double,
                        content_id: String,
                        content_type: String,
                        timestamp: String,
                        date: Date,
                        year: Int,
                        month: Int,
                        week: Int,
                        month_day: Int,
                        hour: Int
                      )

case class CovInputSchema(
                           content_code: String,
                           goal_code: String,
                           goal_name: String,
                           exam_code: String,
                           exam_name: String,
                           subject_code: String,
                           subject_name: String,
                           unit_code: String,
                           unit_name: String,
                           chapter_code: String,
                           chapter_name: String,
                           concept_code: String,
                           concept_name: String
                         )

case class UrlMappedSchema(
                            user_id: String,
                            url: String,
                            referrer: String,
                            user_agent: String,
                            session_id: String,
                            event_code: String,
                            event_name: String,
                            event_type: String,
                            video_state: String,
                            video_url: String,
                            video_time_position: Double,
                            content_id: String,
                            content_type: String,
                            timestamp: String,
                            date: Date,
                            year: Int,
                            month: Int,
                            week: Int,
                            month_day: Int,
                            hour: Int,
                            goal_code: String,
                            goal_name: String,
                            exam_code: String,
                            exam_name: String,
                            subject_code: String,
                            subject_name: String,
                            unit_code: String,
                            unit_name: String,
                            chapter_code: String,
                            chapter_name: String,
                            concept_code: String,
                            concept_name: String
                          )
